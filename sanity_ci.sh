#!/bin/bash
set -ueo pipefail

export CyRemoteManifestOverride="https://raw.githubusercontent.com/Infineon/mtb-super-manifest/v2.X/mtb-super-manifest-fv2.xml"
CE_NAME="mtb-example-hal-pwm-square-wave"
CE_TARGET="CY8CEVAL-062S2"
$CY_TOOLS_PATHS/project-creator/project-creator-cli --board-id ${CE_TARGET} --app-id ${CE_NAME} --target-dir . --user-app-name mtb_app
cd mtb_app
make -j$(nproc) build CONFIG=Release
